using System.Linq;
using DTO;

namespace UseCases
{
    public class CurrentRoundUseCase
    {
        public RoundDTO GetCurrentRound(MatchDTO match)
        {
            var currentRoundNumber = match.currentRoundNumber;
            return match.rounds.First(x => x.roundNumber == currentRoundNumber);
        }
    }
}