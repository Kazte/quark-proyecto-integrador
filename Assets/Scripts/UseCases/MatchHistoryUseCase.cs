﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DTO;
using Interfaces;
using Services_Locator;

namespace UseCases
{
    public class MatchHistoryUseCase
    {
        private IMatchRepository repository;

        public MatchHistoryUseCase(IMatchRepository repository)
        {
            this.repository = repository;
        }

        public async Task<List<MatchHistoryDTO>> GetMatchesByUserId(string userId)
        {
            var matchHistoryList = new List<MatchHistoryDTO>();

            var matches = await repository.FindMatchesByUserId(userId);

            foreach (var match in matches)
            {
                var matchStatus = MatchHistoryStatus.Win;
                var currentUser = UserServiceLocator.GetUser();
                switch (match.matchStatusId)
                {
                    case 1:
                        matchStatus = match.userOne.userId == currentUser.userId
                            ? MatchHistoryStatus.MyTurn
                            : MatchHistoryStatus.OpponentTurn;
                        break;
                    case 2:
                        matchStatus = match.userTwo.userId == currentUser.userId
                            ? MatchHistoryStatus.MyTurn
                            : MatchHistoryStatus.OpponentTurn;
                        break;
                    case 3:
                        matchStatus = match.userOne.userId == currentUser.userId
                            ? MatchHistoryStatus.Win
                            : MatchHistoryStatus.Loose;
                        break;
                    case 4:
                        matchStatus = match.userTwo.userId == currentUser.userId
                            ? MatchHistoryStatus.Win
                            : MatchHistoryStatus.Loose;
                        break;
                    case 5:
                        matchStatus = MatchHistoryStatus.Tie;
                        break;
                    default:
                        throw new Exception("No corresponde a ningun estado");
                }


                matchHistoryList.Add(new MatchHistoryDTO()
                {
                    matchId = match.matchId,
                    matchStatus = matchStatus,
                    myScore = match.userOne.userId == userId ? match.playerOneScore : match.playerTwoScore,
                    opponentScore = match.userOne.userId == userId ? match.playerTwoScore : match.playerOneScore,
                    opponentName = match.userOne.userId == userId ? match.userTwo.username : match.userOne.username,
                    roundNumber = match.currentRoundNumber,
                    isClosed = match.isClosed
                });
            }

            return matchHistoryList;
        }
    }
}