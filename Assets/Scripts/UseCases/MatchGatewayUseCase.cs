﻿using System.Threading.Tasks;
using DTO;
using Interfaces;

namespace UseCases
{
    public class MatchGatewayUseCase
    {
        private IMatchRepository matchRepository;

        public MatchGatewayUseCase(IMatchRepository matchRepository)
        {
            this.matchRepository = matchRepository;
        }

        public async Task<MatchDTO> FindMatch()
        {
            return await matchRepository.FindMatch();
        }

        public async Task<MatchDTO> CheckFindMatchStatus()
        {
            return await matchRepository.CheckFindMatchStatus();
        }

        public async Task<MatchDTO> GetMatchByMatchId(int matchId)
        {
            return await matchRepository.GetMatchByMatchId(matchId);
        }

        public async Task CloseRound(int matchId)
        {
            await matchRepository.CloseRound(matchId);
        }
    }
}