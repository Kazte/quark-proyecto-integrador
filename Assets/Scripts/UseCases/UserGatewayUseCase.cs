using System;
using System.Threading.Tasks;
using DTO;
using Global;
using Interfaces;
using Interfaces.View;

namespace UseCases
{
    public class UserGatewayUseCase
    {
        private IUserRepository repository;

        public UserGatewayUseCase(IUserRepository repository)
        {
            this.repository = repository;
        }

        public async Task<UserDTO> GetUserByEmailAndPassword(string username, string password)
        {
            var response = await repository.GetUserByEmailAndPassword(username, password);


            switch (response.responseCode)
            {
                case 0:
                    break;

                case 1:
                    throw new Exception(GlobalVariables.USER_ALREADY_LOGGED_TEXT);
            }

            return response.userOutput;
        }

        public async Task<UserDTO> UpdateUserIsFinding(bool isFinding)
        {
            return await repository.UpdateUserIsFinding(isFinding);
        }

        public async Task<bool> Logout(string userId)
        {
            return await repository.LogoutUser(userId);
        }

        public async Task<UserDTO> GetUserById(string userId)
        {
            return await repository.GetUserById(userId);
        }

        public async Task RegisterUser(RegisterParams registerParams)
        {
            var response = await repository.RegisterUser(registerParams);

            switch (response.responseCode)
            {
                case 0:
                    break;

                case 1:
                    throw new Exception(GlobalVariables.EMAIL_ERROR_TEXT);

                case 2:
                    throw new Exception(GlobalVariables.USERNAME_ERROR_TEXT);

                case 3:
                    throw new Exception(GlobalVariables.PASSWORD_ERROR_TEXT);
            }
        }
    }
}