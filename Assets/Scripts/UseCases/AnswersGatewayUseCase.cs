﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTO;
using Interfaces;
using Repositories.Answers;

namespace UseCases
{
    public class AnswersGatewayUseCase
    {
        private IAnswerRepository answerRepository;

        public AnswersGatewayUseCase(IAnswerRepository answerRepository)
        {
            this.answerRepository = answerRepository;
        }

        public async Task<MatchDTO> AddAnswers(List<Tuple<int, string>> idCategoriesAnswersTuple, int roundId,
            string userId)
        {
            var answersJson = idCategoriesAnswersTuple.Select(t => new AnswerJson()
            {
                answer = t.Item2,
                categoryId = t.Item1,
                roundId = roundId,
                userId = userId
            }).ToList();

            return await answerRepository.AddAnswers(answersJson);
        }
    }
}