using System.Linq;
using DTO;

namespace UseCases
{
    public class CalculateRoundScoreUseCase
    {
        public int CalculateScore(RoundDTO round, string userId)
        {
            var userAnswers = round.answers.Where(x => x.userId == userId).ToList();

            var userScore = userAnswers.Count(x => x.isRight);

            return userScore;
        }
    }
}