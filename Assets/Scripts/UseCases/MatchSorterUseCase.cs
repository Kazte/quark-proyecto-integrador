﻿using System.Collections.Generic;
using System.Linq;
using DTO;

namespace UseCases
{
    public class MatchSorterUseCase
    {
        public List<MatchHistoryDTO> Sort(List<MatchHistoryDTO> matchList)
        {
            var matchHistorySorted = matchList
                .OrderBy(x => x.isClosed)
                .ThenByDescending(x => x.roundNumber)
                .ThenByDescending(x => x.matchId);


            return matchHistorySorted.ToList();
        }
    }
}