using System.Threading.Tasks;
using DTO;
using Interfaces.View;

namespace Interfaces
{
    public interface IUserRepository
    {
        Task<LoginResponse> GetUserByEmailAndPassword(string email, string password);
        Task<UserDTO> UpdateUserIsFinding(bool isFinding);
        Task<bool> LogoutUser(string userId);
        Task<UserDTO> GetUserById(string userId);
        Task<RegisterValidatorResponse> RegisterUser(RegisterParams registerParams);
    }
}