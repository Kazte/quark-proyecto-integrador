﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DTO;
using Repositories.Answers;

namespace Interfaces
{
    public interface IAnswerRepository
    {
        Task<MatchDTO> AddAnswers(List<AnswerJson> answerJsons);
    }
}