using System.Collections.Generic;
using System.Threading.Tasks;
using DTO;

namespace Interfaces
{
    public interface IMatchRepository
    {
        Task<MatchDTO> FindMatch();
        Task<MatchDTO> CheckFindMatchStatus();
        Task<List<MatchDTO>> FindMatchesByUserId(string userId);
        Task<MatchDTO> GetMatchByMatchId(int matchId);
        Task CloseRound(int matchId);
    }
}