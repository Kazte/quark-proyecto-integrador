﻿using System;
using DTO;
using Utilities.Enums;

namespace Interfaces.View
{
    public interface IMatchResultView
    {
        event Action OnStart;
        event Action OnEnd;
        event Action OnBack;
        event Action OnRematch;

        void SetRounds(RoundResultDTO[] rounds);
        void SetScore(int myScore, int opponentScore);
        void SetResult(string result);
        void ChangeScene(ViewEnum viewEnum);
    }
}