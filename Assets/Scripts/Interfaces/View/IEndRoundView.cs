﻿using System;
using Utilities.Enums;

namespace Interfaces.View
{
    public interface IEndRoundView
    {
        event Action OnStart;
        event Action OnEnd;
        event Action OnNext;
        void SetRound(int roundNumber);
        void SetCategories(string[] categories);
        void SetPlayerWins(int playerOne, int playerTwo);
        void SetAnswersPlayerOne(string[] answers, bool[] isRight);
        void SetAnswersPlayerTwo(string[] answers, bool[] isRight);
        void ChangeView(ViewEnum viewEnum);
        void ClearInputs();
    }
}