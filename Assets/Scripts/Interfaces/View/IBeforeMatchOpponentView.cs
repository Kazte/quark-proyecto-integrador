using System;
using Utilities.Enums;

namespace Interfaces.View
{
    public interface IBeforeMatchOpponentView
    {
        event Action OnStart;
        event Action OnEnd;
        event Action OnNextButtonPressed;

        void ChangeScene(ViewEnum viewEnum);

        void Clear();

        void SetButtonText(string buttonText);

        void SetOpponentNickname(string opponentNickname);

        void SetOpponentWins(int opponentWins);
    }
}