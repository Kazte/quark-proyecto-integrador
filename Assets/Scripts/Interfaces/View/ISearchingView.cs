﻿using System;
using Utilities.Enums;

namespace Interfaces.View
{
    public interface ISearchingView
    {
        event Action OnStart;
        event Action OnEnd;
        event Action OnCancelSearch;
        void ChangeScene(ViewEnum viewEnum);
        void CancelMatch();
    }
}