﻿using System;
using Utilities.Enums;

namespace Interfaces.View
{
    public interface IMatchView
    {
        event Action OnStart;
        event Action OnEnd;
        event Action<string[]> OnReady;
        void SetLetter(string letter);
        void SetCategories(string[] categories);
        void SetRound(int roundNumber);
        void SetTime(int time);
        void ClearInputs();
        void Ready();
        void ChangeScene(ViewEnum viewEnum);
        void SetInteractable(bool set);
        void SetCoins(int coins);

        void PlayTimer();
        void PauseTimer();
    }
}