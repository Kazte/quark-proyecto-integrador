﻿using System;
using System.Collections.Generic;
using DTO;
using Utilities.Enums;

namespace Interfaces.View
{
    public interface IHomeView
    {
        event Action OnStart;
        event Action OnStartNewGame;
        event Action<MatchHistoryDTO> OnHistoryMatchClicked;
        event Action OnHistoryCheck;
        event Action OnUserCheck;
        event Action OnLogout;
        void StartNewGame();
        void SetNickname(string username);
        void SetWins(int wins);
        void LogoutButton();
        void UpdateMatchHistory(List<MatchHistoryDTO> matchesHistory);
        void ChangeScene(ViewEnum viewEnum);
        void SetCoins(int coins);
        void ClearHistoryMatch();
        void SetInteractable(bool set);
    }
}