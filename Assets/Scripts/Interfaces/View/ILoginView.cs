using System;
using Utilities.Enums;

namespace Interfaces.View
{
    public interface ILoginView
    {
        event Action<string, string> OnLogin;
        event Action OnStart;
        event Action OnGoToRegister;

        void SetInteractable(bool set);
        void Login();

        void SetErrorText(string errorText);
        void CleanInputs();

        void ChangeScene(ViewEnum viewEnum);
    }
}