﻿using System;
using Utilities.Enums;

namespace Interfaces.View
{
    public interface IRegisterView
    {
        event Action OnStart;
        event Action OnEnd;
        event Action OnBackToLogin;
        event Action<RegisterParams> OnRegister;

        void SetInteractable(bool set);
        void SetErrorText(string errorText);
        void CleanInputs();
        void ChangeScene(ViewEnum viewEnum);
    }

    public class RegisterParams
    {
        public string username { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
}