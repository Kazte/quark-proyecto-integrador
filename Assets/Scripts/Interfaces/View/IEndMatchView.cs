﻿using System;
using Utilities.Enums;

namespace Interfaces.View
{
    public interface IEndMatchView
    {
        event Action OnStart;
        event Action OnEnd;
        event Action OnNextButton;
        void SetPlayerWins(int playerOne, int playerTwo);
        void SetWinnerText(string winnerText);
        void ChangeScene(ViewEnum viewEnum);
    }
}