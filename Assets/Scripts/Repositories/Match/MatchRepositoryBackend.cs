﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using DTO;
using Interfaces;
using Newtonsoft.Json;
using Services_Locator;

namespace Repositories.Match
{
    public class MatchRepositoryBackend : IMatchRepository
    {
        private HttpClient client;

        private const string BASE_URL = "http://brunofranco-001-site1.etempurl.com/api"; 

        public MatchRepositoryBackend()
        {
            client = new HttpClient();
        }

        public async Task<MatchDTO> FindMatch()
        {
            var user = UserServiceLocator.GetUser();


            using (var request = new HttpRequestMessage(new HttpMethod("Get"),
                       $"{BASE_URL}/FindMatch/Match?userOneId={user.userId}"))
            {
                var response = await client.SendAsync(request);

                return await DeserializeMatchDto(response);
            }
        }

        public async Task<MatchDTO> CheckFindMatchStatus()
        {
            var user = UserServiceLocator.GetUser();

            using (var request = new HttpRequestMessage(new HttpMethod("Get"),
                       $"{BASE_URL}/CheckFindMatchStatus/Match?userId={user.userId}"))
            {
                var response = await client.SendAsync(request);

                return await DeserializeMatchDto(response);
            }
        }

        public async Task<List<MatchDTO>> FindMatchesByUserId(string userId)
        {
            var matchesFound = new List<MatchDTO>();

            using (var request = new HttpRequestMessage(new HttpMethod("Get"),
                       $"{BASE_URL}/GetMatchesByUserId/Match?userId={userId}"))
            {
                var response = await client.SendAsync(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var contentResponse = await response.Content.ReadAsStringAsync();

                    matchesFound = JsonConvert.DeserializeObject<List<MatchDTO>>(contentResponse);
                }
            }

            return matchesFound;
        }

        public async Task<MatchDTO> GetMatchByMatchId(int matchId)
        {
            using (var request = new HttpRequestMessage(new HttpMethod("Get"),
                       $"{BASE_URL}/GetMatchByMatchId/Match?matchId={matchId}"))
            {
                var response = await client.SendAsync(request);

                return await DeserializeMatchDto(response);
            }
        }

        public async Task CloseRound(int matchId)
        {
            
            using (var request = new HttpRequestMessage(new HttpMethod("Put"),
                       $"{BASE_URL}/CloseRound/Match?matchId={matchId}"))
            {
                var response = await client.SendAsync(request);
                
                if (response.StatusCode == HttpStatusCode.NotFound)
                    throw new NotMatchFoundException();
            }
        }

        public static async Task<MatchDTO> DeserializeMatchDto(HttpResponseMessage response)
        {
            if (response.StatusCode != HttpStatusCode.OK) return null;

            var contentResponse = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<MatchDTO>(contentResponse);
        }
    }

    public class NotMatchFoundException : Exception
    {
    }
}