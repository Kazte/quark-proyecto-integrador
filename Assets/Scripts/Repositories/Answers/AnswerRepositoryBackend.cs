﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DTO;
using Interfaces;
using Newtonsoft.Json;
using Repositories.Match;

namespace Repositories.Answers
{
    public class AnswerRepositoryBackend : IAnswerRepository
    {
        private HttpClient client;
        
        private const string BASE_URL = "http://brunofranco-001-site1.etempurl.com/api"; 

        public AnswerRepositoryBackend()
        {
            client = new HttpClient();
        }

        public async Task<MatchDTO> AddAnswers(List<AnswerJson> answerJsons)
        {
            using (var request = new HttpRequestMessage(new HttpMethod("POST"), $"{BASE_URL}/Answer"))
            {
                request.Headers.TryAddWithoutValidation("accept", "application/json");

                var jsonUser = JsonConvert.SerializeObject(answerJsons);


                request.Content =
                    new StringContent(jsonUser);
                request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

                var response = await client.SendAsync(request);

                return await MatchRepositoryBackend.DeserializeMatchDto(response);
            }
        }
    }

    public class AnswerJson
    {
        public int categoryId { get; set; }
        public string answer { get; set; }
        public string userId { get; set; }
        public int roundId { get; set; }
    }
}