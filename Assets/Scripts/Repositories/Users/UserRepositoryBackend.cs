﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DTO;
using Interfaces;
using Interfaces.View;
using Newtonsoft.Json;
using Services_Locator;

namespace Repositories.Users
{
    public class UserRepositoryBackend : IUserRepository
    {
        private HttpClient client;

        private const string BASE_URL = "https://f537-190-48-208-154.sa.ngrok.io";

        public UserRepositoryBackend()
        {
            client = new HttpClient();
        }

        public async Task<LoginResponse> GetUserByEmailAndPassword(string email, string password)
        {
            var uri = new Uri($"{BASE_URL}/login?email={email}&password={password}");

            var result = await client.GetAsync(uri);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                var contentResult = await result.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<LoginResponse>(contentResult);
            }

            throw new UserNotFoundException();
        }

        public async Task<UserDTO> UpdateUserIsFinding(bool isFinding)
        {
            using (var request = new HttpRequestMessage(new HttpMethod("PUT"), $"{BASE_URL}/update-user"))
            {
                request.Headers.TryAddWithoutValidation("accept", "application/json");

                var user = UserServiceLocator.GetUser();
                user.isFinding = isFinding;

                var jsonUser = JsonConvert.SerializeObject(user);


                request.Content =
                    new StringContent(jsonUser);
                request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

                var response = await client.SendAsync(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var contentResult = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<UserDTO>(contentResult);
                }
            }

            throw new Exception("No se pudo conectar.");
        }

        public async Task<bool> LogoutUser(string userId)
        {
            using (var request =
                   new HttpRequestMessage(new HttpMethod("GET"), $"{BASE_URL}/logout?userId={userId}"))
            {
                var response = await client.SendAsync(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }

                return false;
            }
        }

        public async Task<UserDTO> GetUserById(string userId)
        {
            var uri = new Uri($"{BASE_URL}/users/{userId}");

            var result = await client.GetAsync(uri);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                var contentResult = await result.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<UserDTO>(contentResult);
            }

            throw new UserNotFoundException();
        }

        public async Task<RegisterValidatorResponse> RegisterUser(RegisterParams registerParams)
        {
            using (var request = new HttpRequestMessage(new HttpMethod("POST"), $"{BASE_URL}/register"))
            {
                request.Headers.TryAddWithoutValidation("accept", "application/json");

                var jsonUser = JsonConvert.SerializeObject(registerParams);


                request.Content =
                    new StringContent(jsonUser);
                request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

                var response = await client.SendAsync(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var resp = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<RegisterValidatorResponse>(resp);
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(await response.Content.ReadAsStringAsync());
                }
            }

            throw new Exception("No se pudo conectar.");
        }
    }

    class UserNotFoundException : Exception
    {
    }
}

public class RegisterValidatorResponse
{
    public int responseCode { get; set; }
}

public class LoginResponse
{
    public int responseCode { get; set; }
    public UserDTO userOutput { get; set; }
}