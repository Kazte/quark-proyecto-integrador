﻿using System;
using Interfaces.View;
using Observers;
using Presenters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities.Enums;

namespace Views
{
    public class LoginView : MonoBehaviour, ILoginView
    {
        public event Action<string, string> OnLogin;
        public event Action OnStart;

        public event Action OnGoToRegister;

        [SerializeField] private GameEvents gameEvents;

        [SerializeField] private Button loginButton;

        [SerializeField] private Button goToRegisterButton;

        [SerializeField] private TMP_InputField emailInputField;

        [SerializeField] private TMP_InputField passwordInputField;

        [SerializeField] private TMP_Text errorText;


        private void Awake()
        {
            LoginPresenter.Build(this);

            loginButton.onClick.AddListener(Login);

            goToRegisterButton.onClick.AddListener(() => { OnGoToRegister?.Invoke(); });
        }

        private void OnEnable()
        {
            OnStart?.Invoke();
        }

        public void SetInteractable(bool set)
        {
            loginButton.interactable = set;
            emailInputField.interactable = set;
            passwordInputField.interactable = set;
            goToRegisterButton.interactable = set;
        }

        public void Login()
        {
            OnLogin?.Invoke(emailInputField.text, passwordInputField.text);
        }

        public void SetErrorText(string errorText)
        {
            this.errorText.SetText(errorText);
        }

        public void CleanInputs()
        {
            loginButton.interactable = true;
            emailInputField.SetTextWithoutNotify("");
            passwordInputField.SetTextWithoutNotify("");
            emailInputField.interactable = true;
            passwordInputField.interactable = true;
            errorText.SetText("");
        }

        public void ChangeScene(ViewEnum viewEnum)
        {
            gameEvents.ActualView.Value = viewEnum;
        }
    }
}