﻿using System;
using Global;
using Interfaces.View;
using Observers;
using Presenters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities.Enums;

namespace Views
{
    public class SearchingView : MonoBehaviour, ISearchingView
    {
        public event Action OnStart;
        public event Action OnEnd;
        public event Action OnCancelSearch;


        [SerializeField] private GameEvents gameEvents;

        [SerializeField] private Button cancelSearchingButton;

        [SerializeField] private TMP_Text searchingMatchText;


        private void OnEnable()
        {
            SearchingPresenter.Build(this);

            cancelSearchingButton.onClick.AddListener(CancelMatch);
            OnStart?.Invoke();
            
            searchingMatchText.SetText(GlobalVariables.SEARCHING_MATCH);
        }

        private void OnDisable()
        {
            OnEnd?.Invoke();
            cancelSearchingButton.onClick.RemoveAllListeners();
        }

        public void ChangeScene(ViewEnum viewEnum)
        {
            gameEvents.ActualView.Value = viewEnum;
        }

        public void CancelMatch()
        {
            OnCancelSearch?.Invoke();
        }
    }
}