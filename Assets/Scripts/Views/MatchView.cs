﻿using System;
using System.Collections.Generic;
using System.Linq;
using Global;
using Interfaces.View;
using Observers;
using Presenters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities.Enums;
using Widgets;

namespace Views
{
    public class MatchView : MonoBehaviour, IMatchView
    {
        public event Action OnStart;
        public event Action OnEnd;
        public event Action<string[]> OnReady;

        [SerializeField] private GameEvents gameEvents;

        [SerializeField] private TextMeshProUGUI letterText;

        [SerializeField] private TextMeshProUGUI roundText;

        [SerializeField] private TextMeshProUGUI coinsText;

        [SerializeField] private List<CategoryItemWidget> categoryItemsWidgets;

        [SerializeField] private TimerWidget timerWidget;

        [SerializeField] private Button readyButton;

        private float currentTime;

        private bool isReady;
        private bool isTimerPaused;

        private void Awake()
        {
            readyButton.onClick.AddListener(Ready);
        }

        private void OnEnable()
        {
            MatchPresenter.Build(this);

            OnStart?.Invoke();

            isReady = false;
        }

        private void OnDisable()
        {
            OnEnd?.Invoke();
        }

        private void Update()
        {
            if (currentTime >= 0)
            {
                if (!isTimerPaused)
                {
                    currentTime -= Time.deltaTime;
                    timerWidget.SetTime(Mathf.RoundToInt(currentTime));
                }
            }
            else
            {
                if (!isReady)
                    Ready();
            }
        }


        public void SetLetter(string letter)
        {
            letterText.text = letter;
        }

        public void SetCategories(string[] categories)
        {
            for (var i = 0; i < categories.Length; i++)
            {
                categoryItemsWidgets[i].Configure(categories[i]);
            }
        }

        public void SetRound(int roundNumber)
        {
            roundText.text = $"{GlobalVariables.ROUND_TEXT} {roundNumber}";
        }

        public void SetTime(int time)
        {
            timerWidget.SetTime(time);
            currentTime = time;
            PlayTimer();
        }

        public void ClearInputs()
        {
            categoryItemsWidgets.ForEach(widget => widget.ClearInput());
        }


        public void Ready()
        {
            isReady = true;
            string[] answers = categoryItemsWidgets.Select(catItemWidget => catItemWidget.GetInputText()).ToArray();

            // Tiene que comunicarse a traves de eventos.
            OnReady?.Invoke(answers);
        }

        public void SetInteractable(bool set)
        {
            readyButton.interactable = set;

            foreach (var categoryItemsWidget in categoryItemsWidgets)
            {
                categoryItemsWidget.SetInteractable(set);
            }
        }

        public void SetCoins(int coins)
        {
            coinsText.SetText(coins.ToString());
        }

        public void PlayTimer()
        {
            isTimerPaused = false;
        }

        public void PauseTimer()
        {
            isTimerPaused = true;
        }

        public void ChangeScene(ViewEnum viewEnum)
        {
            gameEvents.ActualView.Value = viewEnum;
        }
    }
}