﻿using System;
using Interfaces.View;
using Observers;
using Presenters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities.Enums;

namespace Views
{
    public class EndMatchView : MonoBehaviour, IEndMatchView
    {
        public event Action OnStart;
        public event Action OnEnd;
        public event Action OnNextButton;

        [SerializeField] private GameEvents gameEvents;

        [SerializeField] private TextMeshProUGUI playerOneWinsText;

        [SerializeField] private TextMeshProUGUI playerTwoWinsText;

        [SerializeField] private TextMeshProUGUI winnerText;

        [SerializeField] private Button endMatchButton;

        private void OnEnable()
        {
            EndMatchPresenter.Build(this);

            OnStart?.Invoke();


            endMatchButton.onClick.AddListener(() => OnNextButton?.Invoke());
        }

        private void OnDisable()
        {
            OnEnd?.Invoke();
            endMatchButton.onClick.RemoveAllListeners();
        }

        public void SetPlayerWins(int playerOne, int playerTwo)
        {
            playerOneWinsText.SetText(playerOne.ToString());
            playerTwoWinsText.SetText(playerTwo.ToString());
        }

        public void SetWinnerText(string winnerText)
        {
            this.winnerText.SetText(winnerText);
        }

        public void ChangeScene(ViewEnum viewEnum)
        {
            gameEvents.ActualView.Value = viewEnum;
        }
    }
}