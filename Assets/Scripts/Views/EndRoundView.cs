﻿using System;
using Global;
using Interfaces.View;
using Observers;
using Presenters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities.Enums;
using Widgets;

namespace Views
{
    public class EndRoundView : MonoBehaviour, IEndRoundView
    {
        public event Action OnStart;
        public event Action OnEnd;
        public event Action OnNext;

        [SerializeField] private GameEvents gameEvents;

        [SerializeField] private TextMeshProUGUI roundText;

        [SerializeField] private CategoryItemContainerLeftAndRightWidget[] categoryItemContainerWidgets;

        [SerializeField] private TextMeshProUGUI playerOneWinsText;

        [SerializeField] private TextMeshProUGUI playerTwoWinsText;

        [SerializeField] private Button nextRoundButton;

        private EndRoundPresenter endRoundPresenter;

        private void OnEnable()
        {
            endRoundPresenter = EndRoundPresenter.Build(this);

            OnStart?.Invoke();

            nextRoundButton.onClick.AddListener(() => { OnNext?.Invoke(); });
        }

        private void OnDisable()
        {
            OnEnd?.Invoke();
            nextRoundButton.onClick.RemoveAllListeners();
        }


        public void SetRound(int roundNumber)
        {
            roundText.SetText($"{GlobalVariables.ROUND_TEXT} {roundNumber}");
        }

        public void SetCategories(string[] categories)
        {
            for (int i = 0; i < categoryItemContainerWidgets.Length; i++)
            {
                categoryItemContainerWidgets[i].SetCategory(categories[i]);
            }
        }

        public void SetPlayerWins(int playerOne, int playerTwo)
        {
            playerOneWinsText.SetText(playerOne.ToString());
            playerTwoWinsText.SetText(playerTwo.ToString());
        }

        public void SetAnswersPlayerOne(string[] answers, bool[] isRight)
        {
            for (int i = 0; i < categoryItemContainerWidgets.Length; i++)
            {
                categoryItemContainerWidgets[i].SetLeftAnswer(answers[i], isRight[i]);
            }
        }

        public void SetAnswersPlayerTwo(string[] answers, bool[] isRight)
        {
            for (int i = 0; i < categoryItemContainerWidgets.Length; i++)
            {
                categoryItemContainerWidgets[i].SetRightAnswer(answers[i], isRight[i]);
            }
        }

        public void ChangeView(ViewEnum viewEnum)
        {
            gameEvents.ActualView.Value = viewEnum;
        }

        public void ClearInputs()
        {
            foreach (var categoryItemAnswerWidget in categoryItemContainerWidgets)
            {
                categoryItemAnswerWidget.ClearInput();
            }
        }
    }
}