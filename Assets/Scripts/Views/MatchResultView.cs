﻿using System;
using System.Collections.Generic;
using DTO;
using UnityEngine;
using Interfaces.View;
using Observers;
using Presenters;
using TMPro;
using UnityEngine.UI;
using Utilities.Enums;
using Widgets;

namespace Views
{
    public class MatchResultView : MonoBehaviour, IMatchResultView
    {
        public event Action OnStart;
        public event Action OnEnd;
        public event Action OnBack;
        public event Action OnRematch;

        [SerializeField] private GameEvents gameEvents;
        [SerializeField] private TextMeshProUGUI resultText;
        [SerializeField] private TextMeshProUGUI myScoreText;
        [SerializeField] private TextMeshProUGUI opponentScoreText;
        [SerializeField] private List<RoundItemResultWidget> itemResultWidgets;

        [SerializeField] private Button rematchButton;
        [SerializeField] private Button backButton;

        private void OnEnable()
        {
            MatchResultPresenter.Build(this);

            OnStart?.Invoke();

            backButton.onClick.AddListener(() => OnBack?.Invoke());
            rematchButton.onClick.AddListener(() => OnRematch?.Invoke());
        }

        private void OnDisable()
        {
            OnEnd?.Invoke();

            backButton.onClick.RemoveAllListeners();
            rematchButton.onClick.RemoveAllListeners();
        }

        public void SetRounds(RoundResultDTO[] rounds)
        {
            for (var i = 0; i < itemResultWidgets.Count; i++)
            {
                var itemResultWidget = itemResultWidgets[i];
                var round = rounds[i];

                itemResultWidget.SetRound(round.roundNumber);

                itemResultWidget.SetLeftScore(round.myScore);

                itemResultWidget.SetRightScore(round.opponentScore);
            }
        }

        public void SetScore(int myScore, int opponentScore)
        {
            myScoreText.SetText(myScore.ToString());
            opponentScoreText.SetText(opponentScore.ToString());
        }

        public void SetResult(string result)
        {
            resultText.SetText(result);
        }

        public void ChangeScene(ViewEnum viewEnum)
        {
            gameEvents.ActualView.Value = viewEnum;
        }
    }
}