﻿using System;
using Interfaces.View;
using Observers;
using Presenters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities.Enums;

namespace Views
{
    public class RegisterView : MonoBehaviour, IRegisterView
    {
        public event Action OnStart;
        public event Action OnEnd;
        public event Action OnBackToLogin;
        public event Action<RegisterParams> OnRegister;

        [SerializeField] private GameEvents gameEvents;

        [SerializeField] private TMP_InputField usernameInputField;
        [SerializeField] private TMP_InputField emailInputField;
        [SerializeField] private TMP_InputField passwordInputField;

        [SerializeField] private TMP_Text errorText;


        [SerializeField] private Button registerButton;
        [SerializeField] private Button goToLoginButton;


        private void OnEnable()
        {
            RegisterPresenter.Build(this);

            registerButton.onClick.AddListener(() =>
            {
                var registerParams = new RegisterParams();

                registerParams.email = emailInputField.text;
                registerParams.password = passwordInputField.text;
                registerParams.username = usernameInputField.text;

                OnRegister?.Invoke(registerParams);
            });

            goToLoginButton.onClick.AddListener(() => OnBackToLogin?.Invoke());

            OnStart?.Invoke();
        }

        private void OnDisable()
        {
            registerButton.onClick.RemoveAllListeners();

            goToLoginButton.onClick.RemoveAllListeners();

            OnEnd?.Invoke();
        }

        public void SetInteractable(bool set)
        {
            usernameInputField.interactable = set;
            emailInputField.interactable = set;
            passwordInputField.interactable = set;
            registerButton.interactable = set;
            goToLoginButton.interactable = set;
        }

        public void SetErrorText(string errorText)
        {
            this.errorText.SetText(errorText);
        }

        public void CleanInputs()
        {
            usernameInputField.interactable = true;
            emailInputField.interactable = true;
            passwordInputField.interactable = true;
            registerButton.interactable = true;
            goToLoginButton.interactable = true;

            usernameInputField.SetTextWithoutNotify("");
            emailInputField.SetTextWithoutNotify("");
            passwordInputField.SetTextWithoutNotify("");
        }

        public void ChangeScene(ViewEnum viewEnum)
        {
            gameEvents.ActualView.Value = viewEnum;
        }
    }
}