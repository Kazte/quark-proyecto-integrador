﻿using System;
using Interfaces.View;
using Observers;
using Presenters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities.Enums;

namespace Views
{
    public class BeforeMatchOpponentView : MonoBehaviour, IBeforeMatchOpponentView

    {
        public event Action OnStart;
        public event Action OnEnd;
        public event Action OnNextButtonPressed;

        [SerializeField] private GameEvents gameEvents;

        [SerializeField] private TMP_Text opponentNicknameText;

        [SerializeField] private TMP_Text opponentWinsText;

        [SerializeField] private Button nextButton;


        private void OnEnable()
        {
            BeforeMatchOpponentPresenter.Build(this);
            
            OnStart?.Invoke();

            nextButton.onClick.AddListener(() => OnNextButtonPressed?.Invoke());
        }

        private void OnDisable()
        {
            OnEnd?.Invoke();
            
            nextButton.onClick.RemoveAllListeners();
        }

        public void Clear()
        {
            opponentNicknameText.SetText("");
            opponentWinsText.SetText("");
        }

        public void SetButtonText(string buttonText)
        {
            nextButton.GetComponentInChildren<TMP_Text>().SetText(buttonText);
        }

        public void SetOpponentNickname(string opponentNickname)
        {
            opponentNicknameText.SetText(opponentNickname);
        }

        public void SetOpponentWins(int opponentWins)
        {
            opponentWinsText.SetText(opponentWins.ToString());
        }


        public void ChangeScene(ViewEnum viewEnum)
        {
            gameEvents.ActualView.Value = viewEnum;
        }
    }
}