﻿using System;
using System.Collections.Generic;
using DTO;
using Global;
using Observers;
using Presenters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities.Enums;
using Widgets;

namespace Views
{
    public class EndRoundUserOneView : MonoBehaviour, IEndRoundUserOneView
    {
        public event Action OnStart;

        public event Action OnReturnHome;

        [SerializeField] private TextMeshProUGUI roundText;

        [SerializeField] private List<CategoryItemContainerSimpleWidget> categoryItemContainers;

        [SerializeField] private Button returnHomeButton;

        [SerializeField] private GameEvents gameEvents;


        private void Awake()
        {
            returnHomeButton.onClick.AddListener(() => { OnReturnHome?.Invoke(); });
        }

        private void OnEnable()
        {
            EndRoundUserOnePresenter.Build(this);

            OnStart?.Invoke();
        }

        public void SetRound(int roundNumber)
        {
            roundText.SetText($"{GlobalVariables.ROUND_TEXT} {roundNumber}");
        }

        public void SetCategories(string[] categories)
        {
            for (int i = 0; i < categories.Length; i++)
            {
                var category = categories[i];
                var categoryItemContainer = categoryItemContainers[i];

                categoryItemContainer.SetCategory(category);
            }
        }

        public void SetAnswers(AnswerDTO[] answers)
        {
            for (int i = 0; i < categoryItemContainers.Count; i++)
            {
                var answer = answers[i];
                var categoryItemContainer = categoryItemContainers[i];

                categoryItemContainer.SetAnswer(answer.answer, answer.isRight);
            }
        }

        public void ClearInputs()
        {
            foreach (var itemContainer in categoryItemContainers)
            {
                itemContainer.ClearInput();
            }
        }

        public void ChangeView(ViewEnum viewEnum)
        {
            gameEvents.ActualView.Value = viewEnum;
        }
    }

    public interface IEndRoundUserOneView
    {
        event Action OnStart;
        event Action OnReturnHome;
        void SetRound(int roundNumber);
        void SetCategories(string[] categories);
        void SetAnswers(AnswerDTO[] answers);
        void ClearInputs();
        void ChangeView(ViewEnum viewEnum);
    }
}