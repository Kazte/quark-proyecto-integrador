﻿using System;
using System.Collections.Generic;
using DTO;
using Interfaces.View;
using Observers;
using Presenters;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Utilities.Enums;
using Widgets;

namespace Views
{
    public class HomeView : MonoBehaviour, IHomeView
    {
        public event Action OnStart;

        public event Action OnHistoryCheck;
        public event Action OnUserCheck;

        public event Action OnStartNewGame;
        public event Action<MatchHistoryDTO> OnHistoryMatchClicked;

        public event Action OnLogout;


        [SerializeField] private GameEvents gameEvents;

        [SerializeField] private TMP_Text nicknameText;

        [SerializeField] private TMP_Text winsText;

        [SerializeField] private TMP_Text coinsText;

        [SerializeField] private Button startNewGame;

        [SerializeField] private Transform matchHistoryListContainer;

        [SerializeField] private MatchHistoryWidget matchHistoryWidgetPrefab;

        [SerializeField] private float checkHistoryTime = 5;

        [SerializeField] private Button logoutButton;

        [SerializeField] private GameObject noMatchesFoundText;

        [SerializeField] private GameObject panel;


        private float lastCheckHistoryTime;

        private void Awake()
        {
            HomePresenter.Build(this);

            startNewGame.onClick.AddListener(StartNewGame);
            logoutButton.onClick.AddListener(LogoutButton);

            lastCheckHistoryTime = 0;

            gameEvents.MatchHistoryDtoSelected.Subscribe(matchHistory =>
            {
                if (matchHistory != null)
                {
                    OnHistoryMatchClicked?.Invoke(matchHistory);
                    gameEvents.MatchHistoryDtoSelected.Value = null;
                }
            });
        }

        private void OnEnable()
        {
            OnStart?.Invoke();
            OnHistoryCheck?.Invoke();
            OnUserCheck?.Invoke();
        }

        private void Update()
        {
            if (lastCheckHistoryTime <= 0)
            {
                OnHistoryCheck?.Invoke();
                OnUserCheck?.Invoke();
                lastCheckHistoryTime = checkHistoryTime;
            }
            else
            {
                lastCheckHistoryTime -= Time.deltaTime;
            }
        }

        public void StartNewGame()
        {
            OnStartNewGame?.Invoke();
        }

        public void SetNickname(string username)
        {
            nicknameText.SetText(username);
        }

        public void SetWins(int wins)
        {
            winsText.SetText(wins.ToString());
        }

        public void LogoutButton()
        {
            OnLogout?.Invoke();
        }

        public void UpdateMatchHistory(List<MatchHistoryDTO> matchesHistory)
        {
            if (matchesHistory.Count > 0)
            {
                noMatchesFoundText.SetActive(false);
                for (int i = 0; i < matchHistoryListContainer.childCount; i++)
                {
                    var children = matchHistoryListContainer.GetChild(i).gameObject;
                    Destroy(children);
                }

                foreach (var matchHistory in matchesHistory)
                {
                    var i = Instantiate(matchHistoryWidgetPrefab, matchHistoryListContainer);
                    i.Instantiate(matchHistory, gameEvents);
                }
            }
            else
            {
                for (int i = 0; i < matchHistoryListContainer.childCount; i++)
                {
                    var children = matchHistoryListContainer.GetChild(i).gameObject;
                    Destroy(children);
                }

                noMatchesFoundText.SetActive(true);
            }
        }

        public void ChangeScene(ViewEnum viewEnum)
        {
            gameEvents.ActualView.Value = viewEnum;
        }

        public void SetCoins(int coins)
        {
            coinsText.SetText(coins.ToString());
        }

        public void ClearHistoryMatch()
        {
            noMatchesFoundText.SetActive(false);
            for (int i = 0; i < matchHistoryListContainer.childCount; i++)
            {
                var children = matchHistoryListContainer.GetChild(i).gameObject;
                Destroy(children);
            }
        }

        public void SetInteractable(bool set)
        {
            startNewGame.interactable = set;

            panel.SetActive(!set);

            logoutButton.interactable = set;
        }
    }
}