﻿using TMPro;
using UnityEngine;

namespace Widgets
{
    public class CategoryItemContainerLeftAndRightWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI categoryText;

        [SerializeField] private CategoryItemAnswerWidget leftAnswerWidget;

        [SerializeField] private CategoryItemAnswerWidget rightAnswerWidget;

        public void SetCategory(string categoryName)
        {
            categoryText.SetText(categoryName);
        }

        public void SetLeftAnswer(string answer, bool isRight)
        {
            leftAnswerWidget.SetAnswer(answer, isRight);
        }

        public void SetRightAnswer(string answer, bool isRight)
        {
            rightAnswerWidget.SetAnswer(answer, isRight);
        }

        public void ClearInput()
        {
            leftAnswerWidget.ClearInput();
            rightAnswerWidget.ClearInput();
        }
    }
}