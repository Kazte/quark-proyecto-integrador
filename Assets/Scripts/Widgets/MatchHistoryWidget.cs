﻿using DTO;
using Global;
using Observers;
using Services_Locator;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Widgets
{
    public class MatchHistoryWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI leftUsernameText;
        [SerializeField] private TextMeshProUGUI rightUsernameText;

        [SerializeField] private TextMeshProUGUI roundNumberText;
        [SerializeField] private TextMeshProUGUI scoreText;

        [SerializeField] private Button actionButton;
        [SerializeField] private TextMeshProUGUI actionButtonText;

        private GameEvents gameEvents;
        private MatchHistoryDTO matchHistoryDto;

        public void Instantiate(MatchHistoryDTO matchHistory, GameEvents gameEvents)
        {
            this.gameEvents = gameEvents;
            matchHistoryDto = matchHistory;

            leftUsernameText.SetText(UserServiceLocator.GetUser().username);
            rightUsernameText.SetText(matchHistory.opponentName);

            roundNumberText.SetText($"{GlobalVariables.ROUND_TEXT} {matchHistory.roundNumber}");

            scoreText.SetText($"{matchHistory.myScore} - {matchHistory.opponentScore}");


            switch (matchHistoryDto.matchStatus)
            {
                case MatchHistoryStatus.OpponentTurn:
                    actionButton.interactable = false;
                    actionButtonText.SetText(GlobalVariables.WAITING_OPPONENT_TEXT);
                    break;
                case MatchHistoryStatus.MyTurn:
                    actionButton.interactable = true;
                    actionButtonText.SetText(GlobalVariables.PLAY_TEXT);
                    break;
                case MatchHistoryStatus.Win:
                case MatchHistoryStatus.Loose:
                case MatchHistoryStatus.Tie:
                default:
                    actionButton.interactable = true;
                    actionButtonText.SetText(GlobalVariables.VIEW_RESULTS_TEXT);
                    break;
            }

            actionButton.onClick.AddListener(() =>
            {
                this.gameEvents.MatchHistoryDtoSelected.Value = matchHistoryDto;
            });


            // onClick.AddListener(() => { this.gameEvents.MatchHistoryDtoSelected.Value = matchHistoryDto; });
        }

        public void SetInteractable(bool set)
        {
            throw new System.NotImplementedException();
        }
    }
}