﻿using Global;
using TMPro;
using UnityEngine;

namespace Widgets
{
    public class RoundItemResultWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI roundText;
        [SerializeField] private TextMeshProUGUI leftScoreText;
        [SerializeField] private TextMeshProUGUI rightScoreText;

        public void SetRound(int round)
        {
            roundText.SetText($"{GlobalVariables.ROUND_TEXT} {round}");
        }

        public void SetLeftScore(int score)
        {
            leftScoreText.SetText(score.ToString());
        }

        public void SetRightScore(int score)
        {
            rightScoreText.SetText(score.ToString());
        }
    }
}