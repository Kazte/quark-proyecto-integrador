﻿using TMPro;
using UnityEngine;

namespace Widgets
{
    public class TimerWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI timerText;

        public void SetTime(int time)
        {
            timerText.text = time.ToString();
        }
    }
}