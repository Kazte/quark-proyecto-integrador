﻿using TMPro;
using UnityEngine;

namespace Widgets
{
    public class CategoryItemContainerSimpleWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI categoryText;

        [SerializeField] private CategoryItemAnswerWidget answerWidget;

        public void SetCategory(string categoryName)
        {
            categoryText.SetText(categoryName);
        }

        public void SetAnswer(string answer, bool isRight)
        {
            answerWidget.SetAnswer(answer, isRight);
        }

        public void ClearInput()
        {
            answerWidget.ClearInput();
        }
    }
}