﻿using TMPro;
using UnityEngine;

namespace Widgets
{
    public class CategoryItemWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI categoryTitleText;

        [SerializeField] private TMP_InputField answerInputField;

        public void Configure(string categoryName)
        {
            categoryTitleText.SetText(categoryName);
        }

        public string GetInputText() => answerInputField.text;

        public string GetCategory() => categoryTitleText.text;

        public void ClearInput()
        {
            answerInputField.text = "";
        }

        public void SetInteractable(bool set)
        {
            answerInputField.interactable = set;
        }
    }
}