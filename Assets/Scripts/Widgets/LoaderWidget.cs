﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Widgets
{
    public class LoaderWidget : MonoBehaviour
    {
        [SerializeField] private Image loaderImage;

        [SerializeField] private float timeDuration = 3f;


        private float alpha = 0f;

        private Color colorA = new Color(1f, 1f, 1f, 0f);

        private Color colorB = new Color(1f, 1f, 1f, 1f);


        private void Start()
        {
            loaderImage.DOColor(colorA, timeDuration).SetLoops(-1, LoopType.Yoyo);
        }
    }
}