﻿using TMPro;
using UnityEngine;

namespace Widgets
{
    public class CategoryItemAnswerWidget : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI answerText;

        [SerializeField] private GameObject answerCheck;

        public void SetAnswer(string leftAnswer, bool isRight = default)
        {
            answerText.SetText(leftAnswer);

            if (isRight)
            {
                answerCheck.transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                answerCheck.transform.GetChild(1).gameObject.SetActive(true);
            }
        }

        public void ClearInput()
        {
            answerCheck.transform.GetChild(0).gameObject.SetActive(false);
            answerCheck.transform.GetChild(1).gameObject.SetActive(false);

            answerText.SetText("");
        }
    }
}