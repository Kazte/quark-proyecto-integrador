﻿using DTO;
using UniRx;
using UnityEngine;
using Utilities.Enums;

namespace Observers
{
    [CreateAssetMenu]
    public class GameEvents : ScriptableObject
    {
        public ReactiveProperty<ViewEnum> ActualView = new ReactiveProperty<ViewEnum>(ViewEnum.Login);
        public ReactiveProperty<MatchHistoryDTO> MatchHistoryDtoSelected = new ReactiveProperty<MatchHistoryDTO>();
    }
}