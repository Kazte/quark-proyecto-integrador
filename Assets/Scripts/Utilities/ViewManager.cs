﻿using Observers;
using UniRx;
using UnityEngine;
using Utilities.Enums;

namespace Utilities
{
    public class ViewManager : MonoBehaviour
    {
        [SerializeField] private GameEvents gameEvents;

        [SerializeField] private ViewDictionary[] viewDictionaries;

        private ViewDictionary currentView;

        private void Awake()
        {
            gameEvents.ActualView.Subscribe(ChangeView);
        }

        private void ChangeView(ViewEnum actualViewDictionary)
        {
            if (actualViewDictionary == null)
                return;

            currentView?.gameObject.SetActive(false);

            foreach (var viewDictionary in viewDictionaries)
            {
                if (viewDictionary.viewEnum != actualViewDictionary)
                    continue;

                currentView = viewDictionary;
                currentView.gameObject.SetActive(true);
                break;
            }
        }
    }
}