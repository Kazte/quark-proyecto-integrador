﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using Utilities.Enums;

namespace Utilities
{
    [Serializable]
    public class ViewDictionary
    {
        public GameObject gameObject;
        [FormerlySerializedAs("sceneEnum")] public ViewEnum viewEnum;
    }
}