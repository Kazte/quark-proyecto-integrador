﻿namespace Utilities.Enums
{
    public enum ViewEnum
    {
        Login,
        Home,
        Searching,
        Match,
        EndRound,
        EndMatch,
        NotCurrentTurn,
        EndRoundUserOne,
        BeforeRound,
        ResultMatch,
        Register
    }
}