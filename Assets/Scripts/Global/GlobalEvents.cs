﻿using System;
using Presenters;
using UnityEngine;

namespace Global
{
    public class GlobalEvents : MonoBehaviour, IGlobalEvents
    {
        public event Action OnStart;
        public event Action OnEnd;
        public event Action OnQuitApp;


        private void OnEnable()
        {
            GlobalEventsPresenter.Build(this);

            OnStart?.Invoke();
        }

        private void OnDisable()
        {
            OnEnd?.Invoke();
        }

        private void OnApplicationQuit()
        {
            OnQuitApp?.Invoke();
        }
    }
}

public interface IGlobalEvents
{
    event Action OnStart;
    event Action OnEnd;
    event Action OnQuitApp;
}