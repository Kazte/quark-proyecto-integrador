﻿namespace Global
{
    public static class GlobalVariables
    {
        // NOTE: Scriptable

        public static string ROUND_TEXT = "Ronda";
        public static string VICTORY_TEXT = "Victoria";
        public static string DEFEAT_TEXT = "Derrota";
        public static string TIE_TEXT = "Empate";
        public static string WIN_TEXT = "GANASTE";
        public static string LOOSE_TEXT = "PERDISTE";
        public static string SEARCHING_MATCH = "Buscando partida";

        // Buttons
        public static string RETURN_TO_HOME_TEXT = "Volver al Inicio";
        public static string PLAY_TEXT = "Jugar";
        public static string WAITING_OPPONENT_TEXT = "Esperando oponente";
        public static string VIEW_RESULTS_TEXT = "Ver Resultado";

        public static int MATCH_TIMER = 60;

        // Error texts
        public static string USER_NOT_FOUND_TEXT = "Usuario no encontrado";
        public static string USER_ALREADY_LOGGED_TEXT = "Usuario ya logeado";
        public static string EMAIL_ERROR_TEXT = "Email Error";
        public static string USERNAME_ERROR_TEXT = "Username Error";
        public static string PASSWORD_ERROR_TEXT = "Password Error";
    }
}