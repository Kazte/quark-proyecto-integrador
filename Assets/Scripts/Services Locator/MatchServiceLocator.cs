﻿using DTO;

namespace Services_Locator
{
    public class MatchServiceLocator
    {
        private static MatchDTO _match;

        public static void SetMatch(MatchDTO match)
        {
            _match = match;
        }

        public static MatchDTO GetMatch() => _match;
    }
}