﻿using DTO;

namespace Services_Locator
{
    public class UserServiceLocator
    {
        private static UserDTO _user;

        public static void SetUser(UserDTO user)
        {
            _user = user;
        }

        public static UserDTO GetUser() => _user;
    }
}