﻿using System.Linq;
using DTO;
using Services_Locator;
using UseCases;
using Utilities.Enums;
using Views;

namespace Presenters
{
    public class EndRoundUserOnePresenter
    {
        private IEndRoundUserOneView view;

        private MatchDTO currentMatch;

        public EndRoundUserOnePresenter(IEndRoundUserOneView view)
        {
            this.view = view;

            view.OnStart += ViewOnStart;
            view.OnReturnHome += ViewOnReturnHome;
        }

        

        public static EndRoundUserOnePresenter Build(IEndRoundUserOneView view)
        {
            return new EndRoundUserOnePresenter(view);
        }

        private void ViewOnStart()
        {
            currentMatch = MatchServiceLocator.GetMatch();
            var user = UserServiceLocator.GetUser();

            var currentRound = new CurrentRoundUseCase().GetCurrentRound(currentMatch);

            view.ClearInputs();

            var categories = currentRound.categories.Select(x => x.categoryName).ToArray();
            view.SetCategories(categories);

            var answers = currentRound.answers.Where(x => x.userId == user.userId).ToArray();
            view.SetAnswers(answers);

            view.SetRound(currentRound.roundNumber);
        }
        
        private void ViewOnReturnHome()
        {
            currentMatch = null;
            MatchServiceLocator.SetMatch(null);
            view.ChangeView(ViewEnum.Home);
        }
    }
}