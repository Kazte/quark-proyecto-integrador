﻿using Global;
using Interfaces.View;
using Services_Locator;
using Utilities.Enums;

namespace Presenters
{
    public class BeforeMatchOpponentPresenter
    {
        private IBeforeMatchOpponentView view;

        private bool isMyTurn;

        public BeforeMatchOpponentPresenter(IBeforeMatchOpponentView view)
        {
            this.view = view;

            this.view.OnStart += ViewOnStart;
            this.view.OnEnd += ViewOnEnd;
            this.view.OnNextButtonPressed += ViewOnNextButtonPressed;
        }

        private void ViewOnStart()
        {
            view.Clear();

            var match = MatchServiceLocator.GetMatch();
            var user = UserServiceLocator.GetUser();

            var opponent = match.userOne.userId == user.userId ? match.userTwo : match.userOne;

            if ((match.matchStatusId == 1 && match.userOne.userId != user.userId) ||
                (match.matchStatusId == 2 && match.userTwo.userId != user.userId))
            {
                view.SetButtonText(GlobalVariables.RETURN_TO_HOME_TEXT);
                isMyTurn = false;
            }
            else
            {
                view.SetButtonText(GlobalVariables.PLAY_TEXT);
                isMyTurn = true;
            }

            view.SetOpponentNickname(opponent.username);
            view.SetOpponentWins(opponent.wins);
        }

        private void ViewOnEnd()
        {
            view.OnStart -= ViewOnStart;
            view.OnEnd -= ViewOnEnd;
            view.OnNextButtonPressed -= ViewOnNextButtonPressed;
        }

        private void ViewOnNextButtonPressed()
        {
            if (isMyTurn)
            {
                view.ChangeScene(ViewEnum.Match);
            }
            else
            {
                MatchServiceLocator.SetMatch(null);
                view.ChangeScene(ViewEnum.Home);
            }
        }


        public static BeforeMatchOpponentPresenter Build(IBeforeMatchOpponentView view)
        {
            return new BeforeMatchOpponentPresenter(view);
        }
    }
}