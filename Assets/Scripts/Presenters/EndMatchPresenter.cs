﻿using Global;
using Interfaces.View;
using Repositories.Match;
using Services_Locator;
using UseCases;
using Utilities.Enums;

namespace Presenters
{
    public class EndMatchPresenter
    {
        private IEndMatchView view;

        private MatchGatewayUseCase matchGatewayUseCase;

        private EndMatchPresenter(IEndMatchView view)
        {
            this.view = view;

            this.view.OnStart += ViewOnStart;
            this.view.OnEnd += ViewOnEnd;
            this.view.OnNextButton += ReturnToHome;

            matchGatewayUseCase = new MatchGatewayUseCase(new MatchRepositoryBackend());
        }

        public static EndMatchPresenter Build(IEndMatchView view)
        {
            return new EndMatchPresenter(view);
        }

        public async void ViewOnStart()
        {
            var match = await matchGatewayUseCase.GetMatchByMatchId(MatchServiceLocator.GetMatch().matchId);
            var currentUser = UserServiceLocator.GetUser();

            var myScore = currentUser.userId == match.userOne.userId ? match.playerOneScore : match.playerTwoScore;
            var opponentScore =
                currentUser.userId == match.userOne.userId ? match.playerTwoScore : match.playerOneScore;


            view.SetPlayerWins(myScore, opponentScore);

            if (myScore > opponentScore)
            {
                view.SetWinnerText(GlobalVariables.WIN_TEXT);
            }
            else if (myScore < opponentScore)
            {
                view.SetWinnerText(GlobalVariables.LOOSE_TEXT);
            }
            else
            {
                view.SetWinnerText(GlobalVariables.TIE_TEXT);
            }
        }

        private void ViewOnEnd()
        {
            view.OnStart -= ViewOnStart;
        }

        private void ReturnToHome()
        {
            MatchServiceLocator.SetMatch(null);
            view.ChangeScene(ViewEnum.Home);
        }
    }
}