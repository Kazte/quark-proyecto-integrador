﻿using System;
using DTO;
using Interfaces.View;
using Repositories.Match;
using Repositories.Users;
using Services_Locator;
using UnityEngine;
using UseCases;
using Utilities.Enums;

namespace Presenters
{
    public class HomePresenter
    {
        private IHomeView view;

        private MatchHistoryUseCase matchHistoryUseCase;
        private MatchGatewayUseCase matchGatewayUseCase;
        private MatchSorterUseCase matchSorterUseCase;
        private readonly UserGatewayUseCase userGatewayUseCase;

        public HomePresenter(IHomeView view)
        {
            this.view = view;

            view.OnStart += ViewOnStart;
            view.OnStartNewGame += ViewOnStartNewGame;
            view.OnHistoryCheck += ViewOnHistoryCheck;
            view.OnUserCheck += ViewOnUserCheck;
            view.OnHistoryMatchClicked += ViewOnHistoryMatchClicked;
            view.OnLogout += ViewOnLogout;

            var matchRepository = new MatchRepositoryBackend();
            matchHistoryUseCase = new MatchHistoryUseCase(matchRepository);
            matchGatewayUseCase = new MatchGatewayUseCase(matchRepository);
            matchSorterUseCase = new MatchSorterUseCase();

            userGatewayUseCase = new UserGatewayUseCase(new UserRepositoryBackend());
        }

        private async void ViewOnHistoryMatchClicked(MatchHistoryDTO matchHistory)
        {
            view.SetInteractable(false);
            // Ir a buscar el match por el id
            try
            {
                var matchFound = await matchGatewayUseCase.GetMatchByMatchId(matchHistory.matchId);
                var user = UserServiceLocator.GetUser();
                // Comprobar si es tu turno
                if ((matchFound.matchStatusId == 1 && matchFound.userOne.userId != user.userId) ||
                    (matchFound.matchStatusId == 2 && matchFound.userTwo.userId != user.userId))
                {
                    return;
                }

                // Cambiar el match en el service locator
                MatchServiceLocator.SetMatch(matchFound);

                // Cambiar de escena a match
                if (matchFound.matchStatusId > 2)
                {
                    view.ChangeScene(ViewEnum.ResultMatch);
                }
                else
                {
                    if (matchFound.matchStatusId == 2)
                    {
                        view.ChangeScene(ViewEnum.Match);
                        return;
                    }

                    view.ChangeScene(matchFound.currentRoundNumber > 1 ? ViewEnum.BeforeRound : ViewEnum.Match);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                view.SetInteractable(true);
                throw;
            }
        }

        private async void ViewOnHistoryCheck()
        {
            var user = UserServiceLocator.GetUser();
            var matchesHistory = await matchHistoryUseCase.GetMatchesByUserId(user.userId);

            var matchesHistorySorted = matchSorterUseCase.Sort(matchesHistory);

            view.UpdateMatchHistory(matchesHistorySorted);
        }

        private void ViewOnStartNewGame()
        {
            view.ChangeScene(ViewEnum.Searching);
        }

        private void ViewOnStart()
        {
            view.ClearHistoryMatch();

            var user = UserServiceLocator.GetUser();

            view.SetNickname(user.username);
            view.SetWins(user.wins);
            view.SetCoins(user.coins);
            view.ClearHistoryMatch();
            view.SetInteractable(true);
        }

        private async void ViewOnLogout()
        {
            var user = UserServiceLocator.GetUser();

            try
            {
                var wasLogout = await userGatewayUseCase.Logout(user.userId);

                if (wasLogout)
                {
                    UserServiceLocator.SetUser(null);
                    view.ChangeScene(ViewEnum.Login);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private async void ViewOnUserCheck()
        {
            var user = UserServiceLocator.GetUser();

            try
            {
                var newUser = await userGatewayUseCase.GetUserById(user.userId);

                UserServiceLocator.SetUser(newUser);

                view.SetNickname(newUser.username);
                view.SetWins(newUser.wins);
                view.SetCoins(newUser.coins);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public static HomePresenter Build(IHomeView view)
        {
            return new HomePresenter(view);
        }
    }
}