﻿using System.Linq;
using DTO;
using Interfaces.View;
using Services_Locator;
using Utilities.Enums;

namespace Presenters
{
    public class BeforeRoundPresenter
    {
        private IEndRoundView view;
        private MatchDTO currentMatch;

        public BeforeRoundPresenter(IEndRoundView view)
        {
            this.view = view;

            view.OnStart += OnViewStart;
            view.OnEnd += OnViewEnd;
            view.OnNext += OnViewNext;
        }

        private void OnViewStart()
        {
            currentMatch = MatchServiceLocator.GetMatch();

            view.ClearInputs();

            SetCategories();
            SetAnswers();
            SetScores();
            SetRound();
        }

        private void OnViewEnd()
        {
            view.OnStart -= OnViewStart;
            view.OnEnd -= OnViewEnd;
            view.OnNext -= OnViewNext;
        }

        private void OnViewNext()
        {
            view.ChangeView(ViewEnum.Match);
        }

        private void SetCategories()
        {
            var roundIndex = currentMatch.currentRoundNumber - 2;
            var round = currentMatch.rounds[roundIndex];
            var categoriesName = round.categories.Select(x => x.categoryName).ToArray();

            view.SetCategories(categoriesName);
        }

        private void SetAnswers()
        {
            var user = UserServiceLocator.GetUser();

            var myUserId = currentMatch.userOne.userId == user.userId
                ? currentMatch.userOne.userId
                : currentMatch.userTwo.userId;
            var opponentUserId =
                currentMatch.userOne.userId == user.userId ? currentMatch.userTwo.userId : currentMatch.userOne.userId;

            var roundIndex = currentMatch.currentRoundNumber - 2;

            var myAnswers = currentMatch.rounds[roundIndex].answers.Where(ans => ans.userId == myUserId)
                .Select(ans => ans.answer).ToArray();

            var myAnswersIsRight = currentMatch.rounds[roundIndex].answers.Where(ans => ans.userId == myUserId)
                .Select(ans => ans.isRight).ToArray();

            view.SetAnswersPlayerOne(myAnswers, myAnswersIsRight);

            var opponentAnswers = currentMatch.rounds[roundIndex].answers
                .Where(ans => ans.userId == opponentUserId)
                .Select(ans => ans.answer).ToArray();

            var opponentAnswersIsRight = currentMatch.rounds[roundIndex].answers
                .Where(ans => ans.userId == opponentUserId)
                .Select(ans => ans.isRight).ToArray();

            view.SetAnswersPlayerTwo(opponentAnswers, opponentAnswersIsRight);
        }

        private void SetScores()
        {
            var user = UserServiceLocator.GetUser();

            var myScore = currentMatch.userOne.userId == user.userId
                ? currentMatch.playerOneScore
                : currentMatch.playerTwoScore;
            var opponentScore =
                currentMatch.userOne.userId == user.userId ? currentMatch.playerTwoScore : currentMatch.playerOneScore;

            view.SetPlayerWins(myScore, opponentScore);
        }

        private void SetRound()
        {
            var roundNumber = currentMatch.currentRoundNumber - 1;

            view.SetRound(roundNumber);
        }

        public static BeforeRoundPresenter Build(IEndRoundView view)
        {
            return new BeforeRoundPresenter(view);
        }
    }
}