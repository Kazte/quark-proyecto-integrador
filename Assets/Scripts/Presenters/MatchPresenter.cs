﻿using System;
using System.Collections.Generic;
using System.Linq;
using DTO;
using Global;
using Interfaces.View;
using Repositories.Answers;
using Services_Locator;
using UseCases;
using Utilities.Enums;

namespace Presenters
{
    public class MatchPresenter
    {
        private MatchDTO currentMatch;

        private IMatchView view;

        public MatchPresenter(IMatchView view)
        {
            this.view = view;

            this.view.OnStart += ViewOnStart;
            this.view.OnReady += ViewOnEndRound;
            this.view.OnEnd += ViewOnEnd;
        }

        public static MatchPresenter Build(IMatchView matchView)
        {
            return new MatchPresenter(matchView);
        }

        public void ViewOnStart()
        {
            view.ClearInputs();
            view.SetInteractable(true);
            view.PlayTimer();
            currentMatch = MatchServiceLocator.GetMatch();

            var currentRound = new CurrentRoundUseCase().GetCurrentRound(currentMatch);

            var categories = currentRound.categories.Select(x => x.categoryName).ToArray();


            view.SetCategories(categories);
            view.SetLetter(currentRound.letter);
            view.SetRound(currentRound.roundNumber);
            view.SetTime(GlobalVariables.MATCH_TIMER);
            view.SetCoins(UserServiceLocator.GetUser().coins);
        }

        public void ViewOnEnd()
        {
            view.OnStart -= ViewOnStart;
            view.OnReady -= ViewOnEndRound;
            view.OnEnd -= ViewOnEnd;
        }


        public async void ViewOnEndRound(string[] answers)
        {
            view.SetInteractable(false);
            view.PauseTimer();
            var userDto = UserServiceLocator.GetUser();

            // Mandar las respuestas al backend
            var answersGatewayUseCase = new AnswersGatewayUseCase(new AnswerRepositoryBackend());

            var answersTupleList = new List<Tuple<int, string>>();

            var currentRound = new CurrentRoundUseCase().GetCurrentRound(currentMatch);

            for (int i = 0; i < answers.Length; i++)
            {
                var categoryId = currentRound.categories.Select(x => x.idCategory).ToArray()[i];

                answersTupleList.Add(new Tuple<int, string>(categoryId, answers[i]));
            }

            try
            {
                MatchServiceLocator.SetMatch(await answersGatewayUseCase.AddAnswers(answersTupleList,
                    currentRound.idRound, userDto.userId));

                view.ChangeScene(currentMatch.userOne.userId == userDto.userId
                    ? ViewEnum.EndRoundUserOne
                    : ViewEnum.EndRound);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}