﻿using System;
using System.Collections.Generic;
using DTO;
using Global;
using Interfaces.View;
using Services_Locator;
using UseCases;
using Utilities.Enums;

namespace Presenters
{
    public class MatchResultPresenter
    {
        private IMatchResultView view;
        private MatchDTO currentMatch;

        private CalculateRoundScoreUseCase calculateRoundScoreUseCase;

        public MatchResultPresenter(IMatchResultView view)
        {
            this.view = view;

            this.view.OnStart += OnViewStart;
            this.view.OnEnd += OnViewEnd;
            this.view.OnBack += OnViewBack;

            calculateRoundScoreUseCase = new CalculateRoundScoreUseCase();
        }

        private void OnViewStart()
        {
            currentMatch = MatchServiceLocator.GetMatch();

            SetRounds();
            SetScore();
            SetResult();
        }

        private void OnViewEnd()
        {
            view.OnStart -= OnViewStart;
            view.OnEnd -= OnViewEnd;
            view.OnBack -= OnViewBack;
        }

        private void OnViewBack()
        {
            MatchServiceLocator.SetMatch(null);
            view.ChangeScene(ViewEnum.Home);
        }

        private void SetRounds()
        {
            var roundResults = new List<RoundResultDTO>();

            var myUserId = UserServiceLocator.GetUser().userId;

            var opponentUserId = currentMatch.userOne.userId == myUserId
                ? currentMatch.userTwo.userId
                : currentMatch.userOne.userId;

            foreach (var round in currentMatch.rounds)
            {
                var myScore = calculateRoundScoreUseCase.CalculateScore(round, myUserId);
                var opponentScore = calculateRoundScoreUseCase.CalculateScore(round, opponentUserId);

                roundResults.Add(new RoundResultDTO
                {
                    roundNumber = round.roundNumber,
                    myScore = myScore,
                    opponentScore = opponentScore
                });
            }

            view.SetRounds(roundResults.ToArray());
        }

        private void SetScore()
        {
            var user = UserServiceLocator.GetUser();

            var myScore = currentMatch.userOne.userId == user.userId
                ? currentMatch.playerOneScore
                : currentMatch.playerTwoScore;
            var opponentScore =
                currentMatch.userOne.userId == user.userId ? currentMatch.playerTwoScore : currentMatch.playerOneScore;

            view.SetScore(myScore, opponentScore);
        }

        private void SetResult()
        {
            var resultText = "";
            var user = UserServiceLocator.GetUser();

            switch (currentMatch.matchStatusId)
            {
                case 3:
                    resultText = currentMatch.userOne.userId == user.userId
                        ? GlobalVariables.VICTORY_TEXT
                        : GlobalVariables.DEFEAT_TEXT;
                    break;
                case 4:
                    resultText = currentMatch.userTwo.userId == user.userId
                        ? GlobalVariables.VICTORY_TEXT
                        : GlobalVariables.DEFEAT_TEXT;
                    break;
                case 5:
                    resultText = GlobalVariables.TIE_TEXT;
                    break;
                default:
                    throw new Exception("No corresponde a ningun estado");
            }

            view.SetResult(resultText);
        }

        public static MatchResultPresenter Build(IMatchResultView view)
        {
            return new MatchResultPresenter(view);
        }
    }
}