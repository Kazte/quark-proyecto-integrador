﻿using System;
using Repositories.Users;
using Services_Locator;
using UseCases;

namespace Presenters
{
    public class GlobalEventsPresenter
    {
        private IGlobalEvents globalEvents;
        private UserGatewayUseCase userGatewayUseCase;

        public GlobalEventsPresenter(IGlobalEvents globalEvents)
        {
            userGatewayUseCase = new UserGatewayUseCase(new UserRepositoryBackend());

            this.globalEvents = globalEvents;

            this.globalEvents.OnStart += GlobalEventsOnStart;
            this.globalEvents.OnEnd += GlobalEventsOnEnd;
            this.globalEvents.OnQuitApp += GlobalEventsOnQuitApp;
        }

        private void GlobalEventsOnStart()
        {
        }

        private void GlobalEventsOnEnd()
        {
            globalEvents.OnStart -= GlobalEventsOnStart;
            globalEvents.OnEnd -= GlobalEventsOnEnd;
            globalEvents.OnQuitApp -= GlobalEventsOnQuitApp;
        }

        private async void GlobalEventsOnQuitApp()
        {
            var user = UserServiceLocator.GetUser();
            if (user == null) return;

            try
            {
                var wasLogout = await userGatewayUseCase.Logout(user.userId);

                if (wasLogout)
                {
                    UserServiceLocator.SetUser(null);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        public static GlobalEventsPresenter Build(IGlobalEvents globalEvents)
        {
            return new GlobalEventsPresenter(globalEvents);
        }
    }
}