﻿using System;
using DTO;
using Interfaces.View;
using Repositories.Match;
using Repositories.Users;
using Services_Locator;
using UseCases;
using Utilities.Enums;

namespace Presenters
{
    public class SearchingPresenter
    {
        private ISearchingView view;

        private UserGatewayUseCase userGatewayUseCase;
        private MatchGatewayUseCase matchGatewayUseCase;

        public SearchingPresenter(ISearchingView view)
        {
            userGatewayUseCase = new UserGatewayUseCase(new UserRepositoryBackend());
            matchGatewayUseCase = new MatchGatewayUseCase(new MatchRepositoryBackend());

            this.view = view;

            this.view.OnStart += ViewOnStart;
            this.view.OnEnd += ViewOnEnd;
            this.view.OnCancelSearch += ViewOnCancelSearch;
        }

        private async void ViewOnStart()
        {
            try
            {
                var user = await userGatewayUseCase.UpdateUserIsFinding(true);
                UserServiceLocator.SetUser(user);


                var match = await matchGatewayUseCase.FindMatch();

                if (match == null)
                {
                    // Logica de bucle
                    var userIsFinding = user.isFinding;


                    while (userIsFinding && match == null)
                    {
                        userIsFinding = UserServiceLocator.GetUser().isFinding;

                        match = await matchGatewayUseCase.CheckFindMatchStatus();
                    }
                }

                if (match != null)
                {
                    SetMatch(match);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void ViewOnEnd()
        {
            view.OnStart -= ViewOnStart;
            view.OnEnd -= ViewOnEnd;
            view.OnCancelSearch -= ViewOnCancelSearch;
        }

        private async void ViewOnCancelSearch()
        {
            try
            {
                var user = await userGatewayUseCase.UpdateUserIsFinding(false);
                UserServiceLocator.SetUser(user);
                view.ChangeScene(ViewEnum.Home);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void SetMatch(MatchDTO matchDto)
        {
            MatchServiceLocator.SetMatch(matchDto);
            view.ChangeScene(ViewEnum.NotCurrentTurn);
        }

        public static SearchingPresenter Build(ISearchingView view)
        {
            return new SearchingPresenter(view);
        }
    }
}