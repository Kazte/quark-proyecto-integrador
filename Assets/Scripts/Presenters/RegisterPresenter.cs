﻿using System;
using Interfaces.View;
using Repositories.Users;
using UseCases;
using Utilities.Enums;

namespace Presenters
{
    public class RegisterPresenter
    {
        private IRegisterView view;
        private UserGatewayUseCase userGatewayUseCase;

        public RegisterPresenter(IRegisterView view)
        {
            this.view = view;

            view.OnStart += ViewOnStart;
            view.OnEnd += ViewOnEnd;
            view.OnRegister += ViewOnRegister;
            view.OnBackToLogin += ViewOnBackToLogin;

            userGatewayUseCase = new UserGatewayUseCase(new UserRepositoryBackend());
        }

        private void ViewOnStart()
        {
            view.CleanInputs();
            view.SetErrorText("");
            view.SetInteractable(true);
        }

        private void ViewOnEnd()
        {
            view.OnStart -= ViewOnStart;
            view.OnEnd -= ViewOnEnd;
            view.OnRegister -= ViewOnRegister;
            view.OnBackToLogin -= ViewOnBackToLogin;
        }

        private async void ViewOnRegister(RegisterParams registerParams)
        {
            try
            {
                view.SetInteractable(false);
                await userGatewayUseCase.RegisterUser(registerParams);

                view.ChangeScene(ViewEnum.Login);
            }
            catch (Exception e)
            {
                view.SetInteractable(true);
                view.SetErrorText(e.Message);
                throw;
            }
        }

        private void ViewOnBackToLogin()
        {
            view.CleanInputs();
            view.ChangeScene(ViewEnum.Login);
        }

        public static RegisterPresenter Build(IRegisterView view)
        {
            return new RegisterPresenter(view);
        }
    }
}