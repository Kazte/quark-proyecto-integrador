﻿using System;
using Global;
using Interfaces.View;
using Repositories.Users;
using Services_Locator;
using UseCases;
using Utilities.Enums;

namespace Presenters
{
    public class LoginPresenter
    {
        private ILoginView view;

        private UserGatewayUseCase userGatewayUseCase;

        public LoginPresenter(ILoginView view)
        {
            userGatewayUseCase = new UserGatewayUseCase(new UserRepositoryBackend());

            this.view = view;

            this.view.OnLogin += ViewOnLogin;
            this.view.OnStart += ViewOnStart;
            this.view.OnGoToRegister += ViewOnGoToRegister;
        }

        public static LoginPresenter Build(ILoginView view)
        {
            return new LoginPresenter(view);
        }

        private void ViewOnStart()
        {
            view.CleanInputs();
            view.SetInteractable(true);
        }

        private async void ViewOnLogin(string username, string password)
        {
            view.SetInteractable(false);

            try
            {
                var user = await userGatewayUseCase.GetUserByEmailAndPassword(username, password);

                UserServiceLocator.SetUser(user);

                view.ChangeScene(ViewEnum.Home);
            }
            catch (UserNotFoundException e)
            {
                view.SetErrorText(GlobalVariables.USER_NOT_FOUND_TEXT);
                view.SetInteractable(true);
                throw;
            }
            catch (Exception e)
            {
                view.SetErrorText(e.Message);
                view.SetInteractable(true);
                throw;
            }
        }

        private void ViewOnGoToRegister()
        {
            view.CleanInputs();
            view.ChangeScene(ViewEnum.Register);
        }
    }
}