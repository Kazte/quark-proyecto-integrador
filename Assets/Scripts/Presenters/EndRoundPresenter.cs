﻿using System.Linq;
using DTO;
using Interfaces.View;
using Repositories.Match;
using Services_Locator;
using UseCases;
using Utilities.Enums;

namespace Presenters
{
    public class EndRoundPresenter
    {
        private IEndRoundView view;

        private MatchDTO currentMatch;

        private MatchGatewayUseCase matchGatewayUseCase;

        private EndRoundPresenter(IEndRoundView endRoundView)
        {
            view = endRoundView;

            view.OnStart += ViewOnStart;
            view.OnEnd += ViewOnEnd;
            view.OnNext += NextRound;

            matchGatewayUseCase = new MatchGatewayUseCase(new MatchRepositoryBackend());
        }

        public static EndRoundPresenter Build(IEndRoundView endRoundView)
        {
            return new EndRoundPresenter(endRoundView);
        }

        public void ViewOnStart()
        {
            view.ClearInputs();
            currentMatch = MatchServiceLocator.GetMatch();

            SetCategories();
            SetAnswers();
            SetPlayersWins();
            SetRound();
        }

        private void ViewOnEnd()
        {
            view.OnStart -= ViewOnStart;
            view.OnEnd -= ViewOnEnd;
            view.OnNext -= NextRound;

            matchGatewayUseCase = null;
        }

        private void SetCategories()
        {
            var currentRound = new CurrentRoundUseCase().GetCurrentRound(currentMatch);

            view.ClearInputs();

            var categories = currentRound.categories.Select(x => x.categoryName).ToArray();
            view.SetCategories(categories);
        }

        private void SetAnswers()
        {
            var user = UserServiceLocator.GetUser();

            var myUserId = currentMatch.userOne.userId == user.userId
                ? currentMatch.userOne.userId
                : currentMatch.userTwo.userId;
            var opponentUserId =
                currentMatch.userOne.userId == user.userId ? currentMatch.userTwo.userId : currentMatch.userOne.userId;

            var currentRound = new CurrentRoundUseCase().GetCurrentRound(currentMatch);
            var currentRoundIndex = currentRound.roundNumber - 1;

            var myAnswers = currentMatch.rounds[currentRoundIndex].answers.Where(ans => ans.userId == myUserId)
                .Select(ans => ans.answer).ToArray();

            var myAnswersIsRight = currentMatch.rounds[currentRoundIndex].answers.Where(ans => ans.userId == myUserId)
                .Select(ans => ans.isRight).ToArray();

            view.SetAnswersPlayerOne(myAnswers, myAnswersIsRight);

            var opponentAnswers = currentMatch.rounds[currentRoundIndex].answers
                .Where(ans => ans.userId == opponentUserId)
                .Select(ans => ans.answer).ToArray();

            var opponentAnswersIsRight = currentMatch.rounds[currentRoundIndex].answers
                .Where(ans => ans.userId == opponentUserId)
                .Select(ans => ans.isRight).ToArray();

            view.SetAnswersPlayerTwo(opponentAnswers, opponentAnswersIsRight);
        }

        private void SetPlayersWins()
        {
            var user = UserServiceLocator.GetUser();

            var myScore = currentMatch.userOne.userId == user.userId
                ? currentMatch.playerOneScore
                : currentMatch.playerTwoScore;
            var opponentScore =
                currentMatch.userOne.userId == user.userId ? currentMatch.playerTwoScore : currentMatch.playerOneScore;

            view.SetPlayerWins(myScore, opponentScore);
        }

        private void SetRound()
        {
            var currentRound = new CurrentRoundUseCase().GetCurrentRound(currentMatch);

            view.SetRound(currentRound.roundNumber);
        }

        public async void NextRound()
        {
            var currentRound = new CurrentRoundUseCase().GetCurrentRound(currentMatch);

            if (currentRound.roundNumber < 3)
            {
                await matchGatewayUseCase.CloseRound(currentMatch.matchId);

                view.ChangeView(ViewEnum.Home);
            }
            else
            {
                await matchGatewayUseCase.CloseRound(currentMatch.matchId);

                view.ChangeView(ViewEnum.EndMatch);
            }
        }
    }
}