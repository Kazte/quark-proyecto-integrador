﻿using System;
using Interfaces.View;
using NSubstitute;
using NUnit.Framework;
using Presenters;

namespace Tests
{
    public class LoginPresenterShould
    {
        private LoginPresenter presenter;
        private ILoginView view;

        [SetUp]
        public void Setup()
        {
            view = Substitute.For<ILoginView>();

            presenter = LoginPresenter.Build(view);
        }

        [Test]
        public void CallDisableInputs()
        {
            // Arrange
            view.OnLogin += Raise.Event<Action<string, string>>(Arg.Any<string>(), Arg.Any<string>());

            // Assert
            view.Received(1).SetInteractable(Arg.Any<bool>());
        }
    }
}


// var myAction = Substitute.For<Action<string>>();
// myAction.Invoke("abc");
//
// // assert the Action<string> was called once
// Assert.Equal(1, myAction.ReceivedCalls().Count());
//
// // assert that the first parameter on the first call was "abc"
// Assert.Equal("abc", myAction.ReceivedCalls().First().GetArguments().First());