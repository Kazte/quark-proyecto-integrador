﻿using System.Collections.Generic;
using DTO;
using NUnit.Framework;
using UseCases;

namespace Tests
{
    public class MatchSorterUseCaseShould
    {
        [Test]
        public void Sort_By_CurrentRoundNumber()
        {
            // Arrange
            var matchList = new List<MatchHistoryDTO>
            {
                new MatchHistoryDTO
                {
                    matchId = 1,
                    roundNumber = 2
                },
                new MatchHistoryDTO
                {
                    matchId = 2,
                    roundNumber = 1
                },
                new MatchHistoryDTO
                {
                    matchId = 3,
                    roundNumber = 3
                }
            };

            var matchSorterUseCase = new MatchSorterUseCase();

            // Act
            var matchesSorted = matchSorterUseCase.Sort(matchList);

            // Assert
            Assert.AreEqual(3, matchesSorted[0].matchId);
            Assert.AreEqual(1, matchesSorted[1].matchId);
            Assert.AreEqual(2, matchesSorted[2].matchId);
        }

        [Test]
        public void Sort_By_CurrentRoundNumber_And_MatchId()
        {
            // Arrange
            var matchList = new List<MatchHistoryDTO>
            {
                new MatchHistoryDTO
                {
                    matchId = 1,
                    roundNumber = 1
                },
                new MatchHistoryDTO
                {
                    matchId = 4,
                    roundNumber = 1
                },
            };

            var matchSorterUseCase = new MatchSorterUseCase();

            // Act
            var matchesSorted = matchSorterUseCase.Sort(matchList);

            // Assert
            Assert.AreEqual(4, matchesSorted[0].matchId);
            Assert.AreEqual(1, matchesSorted[1].matchId);
        }

        [Test]
        public void Sort_By_MatchIsClosed()
        {
            // Arrange
            var matchList = new List<MatchHistoryDTO>
            {
                new MatchHistoryDTO
                {
                    matchId = 1,
                    isClosed = true
                },
                new MatchHistoryDTO
                {
                    matchId = 2,
                    isClosed = false
                },
                new MatchHistoryDTO
                {
                    matchId = 3,
                    isClosed = true
                },
            };

            var matchSorterUseCase = new MatchSorterUseCase();

            // Act
            var matchesSorted = matchSorterUseCase.Sort(matchList);

            // Assert
            Assert.AreEqual(2, matchesSorted[0].matchId);
            Assert.AreEqual(3, matchesSorted[1].matchId);
            Assert.AreEqual(1, matchesSorted[2].matchId);
        }
    }
}