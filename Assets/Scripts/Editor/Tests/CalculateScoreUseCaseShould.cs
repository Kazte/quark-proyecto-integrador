﻿using System.Collections.Generic;
using DTO;
using NUnit.Framework;
using UseCases;

namespace Tests
{
    public class CalculateScoreUseCaseShould
    {
        [Test]
        public void CalculateScoreByRoundWithUserId()
        {
            // Arrange
            var answers = new List<AnswerDTO>
            {
                new AnswerDTO
                {
                    isRight = true,
                    userId = "1"
                },
                new AnswerDTO
                {
                    isRight = true,
                    userId = "1"
                },
                new AnswerDTO
                {
                    isRight = false,
                    userId = "1"
                },
                new AnswerDTO
                {
                    isRight = false,
                    userId = "1"
                },
                new AnswerDTO
                {
                    isRight = true,
                    userId = "1"
                },
                new AnswerDTO
                {
                    isRight = true,
                    userId = "2"
                },
                new AnswerDTO
                {
                    isRight = false,
                    userId = "2"
                },
                new AnswerDTO
                {
                    isRight = false,
                    userId = "2"
                },
                new AnswerDTO
                {
                    isRight = false,
                    userId = "2"
                },
                new AnswerDTO
                {
                    isRight = true,
                    userId = "2"
                },
            };
            var round = new RoundDTO
            {
                answers = answers,
            };
            
            var calculateRoundScoreUseCase = new CalculateRoundScoreUseCase();
            
            // Act
            var userId1Score = calculateRoundScoreUseCase.CalculateScore(round, "1");
            
            // Assert
            Assert.AreEqual(3, userId1Score);
        }
    }
}