﻿using System.Collections.Generic;
using DTO;
using NUnit.Framework;
using UseCases;

namespace Tests
{
    public class CurrentRoundUseCaseShould
    {
        [Test]
        public void ReturnCurrentRound()
        {
            // Arrange
            var currentRoundUseCase = new CurrentRoundUseCase();

            var rounds = new List<RoundDTO>()
            {
                new RoundDTO
                {
                    idRound = 1,
                    roundNumber = 1
                },
                new RoundDTO
                {
                    idRound = 2,
                    roundNumber = 2
                },
                new RoundDTO
                {
                    idRound = 3,
                    roundNumber = 3
                }
            };
            var match = new MatchDTO
            {
                rounds = rounds,
                currentRoundNumber = 2
            };

            // Act
            var currentRound = currentRoundUseCase.GetCurrentRound(match);

            // Assert
            Assert.AreEqual(2, currentRound.idRound);
        }
    }
}