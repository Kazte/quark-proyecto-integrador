﻿using System.Threading.Tasks;
using DTO;
using Interfaces;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UseCases;

namespace Tests
{
    public class UserGatewayShould
    {
        // [SetUp]
        // public void SetUp()
        // {
        //     
        // }

        [Test]
        public async void GetUsersByUsername()
        {
            // Arrange
            var userRepository = Substitute.For<IUserRepository>();
            userRepository.GetUserByEmailAndPassword("quark@gmail.com", "123456").Returns(Task.FromResult(
                new LoginResponse
                {
                    responseCode = 0,
                    userOutput = new UserDTO()
                    {
                        userId = "1",
                        username = "Quark",
                        wins = 1
                    }
                }));

            var userGateway = new UserGatewayUseCase(userRepository);

            // Act
            var user = await userGateway.GetUserByEmailAndPassword("Quark", "123456");

            // Assert
            Assert.AreEqual(user.username, "Quark");
        }

        [Test]
        public async void SetUserIsFinding_True()
        {
            // Arrange
            var userRepository = Substitute.For<IUserRepository>();
            userRepository.UpdateUserIsFinding(true).Returns(Task.FromResult(
                new UserDTO()
                {
                    userId = "1",
                    username = "Quark",
                    wins = 1,
                    email = "quark@gmail.com",
                    isFinding = false
                }));

            var userGateway = new UserGatewayUseCase(userRepository);

            // Act
            var user = await userGateway.UpdateUserIsFinding(true);

            // Assert
            // TODO: wtf? pq el test pasa
            Assert.That(user.isFinding, Is.EqualTo(true));
        }
    }
}