﻿using System;
using Interfaces.View;
using NSubstitute;
using NUnit.Framework;
using Presenters;
using Utilities;
using Utilities.Enums;

namespace Tests
{
    public class HomePresenterShould
    {
        [Test]
        public void Logout_When_Press_Logout()
        {
            var view = Substitute.For<IHomeView>();
            
            var presenter = new HomePresenter(view);

            view.OnLogout += Raise.Event<Action>();
            
            view.Received(1).ChangeScene(ViewEnum.Login);
        }
    }
}