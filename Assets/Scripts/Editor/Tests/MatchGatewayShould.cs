﻿using System.Threading.Tasks;
using DTO;
using Interfaces;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UseCases;

namespace Tests
{
    public class MatchGatewayShould
    {
        [Test]
        public async void FindMatch_MatchFound()
        {
            // Arrange
            var matchRepository = Substitute.For<IMatchRepository>();
            matchRepository.FindMatch().Returns(Task.FromResult(
                new MatchDTO()
            ));

            var matchGateway = new MatchGatewayUseCase(matchRepository);

            // Act
            var match = await matchGateway.FindMatch();

            // Arrange
            Assert.IsNotNull(match);
            Assert.AreEqual(typeof(MatchDTO), match.GetType());
        }
    }
}