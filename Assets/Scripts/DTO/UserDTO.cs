﻿using System;

namespace DTO
{
    [Serializable]
    public class UserDTO
    {
        public string userId { get; set; }

        public string email { get; set; }

        public string username { get; set; }

        public int wins { get; set; }

        public bool isFinding { get; set; }

        public int coins { get; set; }
        public bool isLogged { get; set; }

        public override string ToString()
        {
            return $"Id: {userId}, Username: {username}, Email: {email}, Wins {wins}, IsFinding {isFinding}";
        }
    }
}