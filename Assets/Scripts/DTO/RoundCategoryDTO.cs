﻿namespace DTO
{
    public class RoundCategoryDTO
    {
        public int idRound { get; set; }
        public int idCategory { get; set; }
        public string categoryName { get; set; }
    }
}