﻿using System.Collections.Generic;

namespace DTO
{
    public class RoundDTO
    {
        public int idRound { get; set; }
        public int roundNumber { get; set; }
        public string letter { get; set; }
        public List<AnswerDTO> answers { get; set; }
        public List<RoundCategoryDTO> categories { get; set; }
        public bool isClosed { get; set; }
        
    }
}