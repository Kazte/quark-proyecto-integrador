﻿namespace DTO
{
    public class AnswerDTO
    {
        public int idAnswer { get; set; }
        public CategoryDTO category { get; set; }
        public string answer { get; set; }
        public string userId { get; set; }
        public bool isRight { get; set; }
    }
}