﻿namespace DTO
{
    public class CategoryDTO
    {
        public string idCategory { get; set; }
        public string categoryName { get; set; }
    }
}