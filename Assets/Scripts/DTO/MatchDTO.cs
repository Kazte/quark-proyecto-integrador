﻿using System.Collections.Generic;

namespace DTO
{
    public class MatchDTO
    {
        public int matchId { get; set; }
        public List<RoundDTO> rounds { get; set; }
        public int playerOneScore { get; set; }
        public int playerTwoScore { get; set; }
        public int matchStatusId { get; set; }
        public UserDTO userOne { get; set; }
        public UserDTO userTwo { get; set; }
        public int currentRoundNumber { get; set; }
        public bool isClosed { get; set; }
    }
}