﻿namespace DTO
{
    public class MatchHistoryDTO
    {
        public int matchId { get; set; }
        public MatchHistoryStatus matchStatus { get; set; }
        public int myScore { get; set; }
        public int opponentScore { get; set; }
        public string opponentName { get; set; }
        public int roundNumber { get; set; }
        public bool isClosed { get; set; }
    }

    public enum MatchHistoryStatus
    {
        MyTurn,
        OpponentTurn,
        Win,
        Loose,
        Tie
    }
}