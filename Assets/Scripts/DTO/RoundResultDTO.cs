﻿namespace DTO
{
    public class RoundResultDTO
    {
        public int roundNumber { get; set; }
        public int myScore { get; set; }
        public int opponentScore { get; set; }
    }
}