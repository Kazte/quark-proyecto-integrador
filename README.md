# Pantalla de Inicio y Emparejamiento

Al momento de ingresar en el juego, el sistema mostrará la pantalla de inicio, donde el jugador podrá:

A) Visualizar las partidas abiertas e históricas. Es deseable también que el sistema muestre el nombre / nickname del jugador junto a la cantidad de victorias obtenidas.

B) Iniciar una nueva partida, donde será emparejado con otro usuario y darán inicio a una partida de juego que los vincula.

Un usuario será emparejado con otro cuando ambos estén buscando un nuevo oponente.

El criterio para emparejar dos usuarios sigue los siguientes pasos:
Los dos usuarios tienen el mismo número de victorias +/- 5.
Si no se encuentra un usuario que está esperando por una partida en base al criterio 1, entonces se puede tomar un usuario con menos de 5 victorias.
Si no se encuentra un usuario que está esperando por una partida en base al criterio 2, entonces se puede tomar un usuario de manera aleatoria.

C) Continuar una partida abierta
En este caso, el usuario podrá retomar una partida abierta que esté jugando contra otro jugador.

# Partidas

Una partida de juego está conformada por 3 rondas. Una ronda está completa cuando ambos jugadores terminan su turno.

Al final de cada ronda, el jugador que haya obtenido más aciertos en su partida, gana la ronda.

Al final de las 3 rondas, el jugador que tiene más rondas ganadas, gana la partida del juego.

En caso de que ambos jugadores tengan los mismos aciertos, ambos jugadores tendrán como ganada la ronda. Y en caso de que ambos hayan empatado las 3 rondas, entonces la partida termina en empate.

El ganador de una partida, obtiene una victoria que puede ser visualizada en el contador de victorias de la pantalla de inicio del juego.

# Dinámica de juego

Al iniciar una partida, el jugador comienza la primera ronda, donde el sistema otorgará de forma aleatoria 5 categorías y una letra al azar.

En su turno, el jugador tratará de completar las 5 palabras que comiencen con la letra asignada y que estén relacionadas a las categorías otorgadas por el sistema.

El tiempo para completar palabras no es ilimitado, ya que un marcador irá mostrando cuánto tiempo queda para terminar el turno. Generalmente un turno tiene 60 segundos (aunque esta regla de negocio no está escrita en piedra). Una vez que el contador llega a cero, el turno finaliza y el jugador no puede completar más palabras.

Una vez que el jugador completa las palabras (puede completar cero, una o varias), presiona el botón “Hecho” o “Done” para finalizar el turno. En ese momento, el sistema valida las palabras ingresadas y muestra una pantalla con las palabras correctas e incorrectas. Aquí el jugador puede volver a la pantalla de inicio del juego, quedando en espera para que el oponente juegue su turno.

El oponente puede jugar su turno en cualquier momento y finalizar la ronda. No es necesario que los dos jugadores se encuentren online para jugar una partida, ya que el juego tiene una mecánica por turnos de forma asíncrona. En este sentido, pueden conectarse en distintos momentos del día para jugar sus partidas en curso.

Nota: un jugador puede abandonar una partida en curso para retomarla en otro momento. Para abandonar una partida, el turno actual debe ser del oponente; ya que no se puede abandonar una partida mientras se está jugando el turno actual.
Para retomar una partida, el turno actual de juego debe ser del jugador que retoma la partida, (ya que no se podría retomar cuando es el turno del oponente).

Se deberá construir una base de categorías y de palabras asociadas a ellas como punto inicial. Luego, las categorías deberán poder ser extensibles en el tiempo, al igual que las palabras vinculadas con ellas.

## Las categorías iniciales pueden ser:

Frutas y vegetales
Monedas
Objetos
Paises
Bandas musicales
Profesiones
Superpoderes
Lenguajes
Emociones
Elementos de cocina
etc…

Si la palabra que el usuario ingresa para la categoría no está vinculada a la misma, entonces se marca como error. Y si la palabra está vinculada, se marca como acierto.

# ÉPICAS ADICIONALES

Importante: las épicas descritas a continuación no forman parte del MVP (producto mínimo viable) del Proyecto Integrador a desarrollar, sólo están detalladas porque formaron parte de la aplicación original. Aunque no son requeridas, se considera un plus el desarrollo de estas épicas en alguno de los sprints finales del proyecto.
Revancha
Desde la pantalla principal, en la lista de partidas históricas, el jugador puede hacer una revancha con cualquiera de los oponentes con los que ya compitió. En el detalle de cada partida histórica debe haber un botón de “Revancha” (o “Rematch” en inglés) que le permita al jugador iniciar una nueva partida contra dicho oponente.
Power Up's
Un power up es un elemento que sirve de ventaja para un jugador. El mismo se podrá adquirir mediante el uso de monedas.
Inicialmente definiremos un solo tipo de power up:
Acertar palabra. Al momento de competir contra otro jugador, el usuario podrá usar solo 2 veces por ronda, el Power Up de Acertar Palabra. El mismo tiene como efecto completar automáticamente una palabra válida para una categoría. El costo del mismo es de 100 monedas de oro, que serán debitadas de su billetera.

# Economía del juego

El juego tendrá como currency interno la moneda de oro. La misma podrá obtenerse mediante la competencia con otros jugadores y será acreditada en una billetera personal. El uso principal de esta moneda es la compra de power up 's.

El ganador de una partida de juego, además de obtener una victoria también gana 100 monedas de oro que serán asignadas a su billetera personal.
